from django.contrib import admin

from .models import Poll, Question, AnswerOption, PollResult


@admin.register(Poll)
class PollAdmin(admin.ModelAdmin):
    pass


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    pass


@admin.register(AnswerOption)
class AnswerOptionAdmin(admin.ModelAdmin):
    pass


@admin.register(PollResult)
class PollResultAdmin(admin.ModelAdmin):
    pass
