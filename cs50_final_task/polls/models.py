from django.db import models, transaction
from django.conf import settings

from .helpers import truncate_tail


class AnswerOption(models.Model):
    text = models.TextField()
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return truncate_tail(self.text)

    class Meta:
        verbose_name = 'Answer Option'


class Question(models.Model):
    text = models.TextField()
    answer_options = models.ManyToManyField(
        AnswerOption,
        related_name='questions',
    )

    class Meta:
        verbose_name = 'Question'

    def __str__(self):
        return truncate_tail(self.text)


class Poll(models.Model):
    name = models.CharField(max_length=256)
    desc = models.TextField(null=True, blank=True)
    start_date = models.DateField()
    end_date = models.DateField()
    questions = models.ManyToManyField(Question, related_name='polls')

    NON_EDITABLE_FIELDS = ('start_date', )

    class Meta:
        verbose_name = 'Poll'

    def update(self, **kwargs):
        for key, value in kwargs.items():
            # Validate <NON_EDITABLE_FIELDS> are not modified
            if key in self.NON_EDITABLE_FIELDS:
                continue
            setattr(self, key, value)
        self.save(update_fields=kwargs.keys())

    def __str__(self):
        return self.name


class PollResult(models.Model):
    poll = models.ForeignKey(
        Poll,
        on_delete=models.CASCADE,
        related_name='poll_results'
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="poll_results",
    )
    score = models.PositiveIntegerField()

    class Meta:
        verbose_name = 'Poll Result'

    def __str__(self):
        return f'{self.user} - {self.poll}'
