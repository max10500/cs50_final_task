
def truncate_tail(value: str):
    max_length = 20
    return f'{value[:max_length]}...' if len(value) > max_length else value
