from django.urls import path, include

from . import views


urlpatterns = [
    path('', views.poll_list, name='poll-list'),
    path('poll/', include([
        path('<int:pk>/', views.poll, name='poll'),
        path('statistics/', views.poll_statistics, name='poll-statistics')
    ])),
]
