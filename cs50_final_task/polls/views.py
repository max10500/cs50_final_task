from datetime import date

from django.shortcuts import render, get_object_or_404

from .models import Poll, PollResult, Question


def poll_list(request):
    polls = Poll.objects.filter(end_date__gte=date.today())
    if request.user.is_anonymous:
        passed_polls = PollResult.objects.none()
    else:
        passed_polls = [
            poll_result.poll.pk
            for poll_result in PollResult.objects.filter(user=request.user)
        ]
    not_passed_polls = polls.exclude(pk__in=passed_polls)

    return render(request, 'polls/poll_list.html', {'polls': not_passed_polls})


def poll(request, pk):
    poll = get_object_or_404(Poll, pk=pk)

    error_message = None
    if poll.end_date < date.today():
        error_message = "You can't take this poll, it was closed on " \
                        f"{poll.end_date}"

    if poll.start_date > date.today():
        error_message = "You can't take this poll, it will start on " \
                        f"{poll.start_date}"

    user = request.user
    if user.is_authenticated:
        try:
            PollResult.objects.get(poll=poll, user=user)
            error_message = 'You already passed this poll!'
        except PollResult.DoesNotExist:
            pass

    if error_message is not None:
        return render(
            request,
            'polls/poll_error.html',
            {'poll': poll, 'message': error_message}
        )

    questions = poll.questions.all()

    if request.method == 'POST':
        score = 0
        mistaken_questions = []
        data = request.POST.items()

        for key, value in data:
            if "answer_option" not in key:
                continue

            question_pk, is_correct_answer = value.split("_")
            if is_correct_answer == "True":
                score += 1
            else:
                mistaken_question = Question.objects.get(pk=int(question_pk))
                mistaken_questions.append(mistaken_question)

        if user.is_authenticated:
            poll_result = PollResult.objects.create(
                poll=poll,
                score=score,
                user=user,
            )
            poll_result.save()

        data = {
            'poll': poll,
            'score': score,
            'mistaken_questions': mistaken_questions,
        }

        return render(request, 'polls/poll_result.html', data)

    return render(
        request,
        'polls/poll.html',
        {'poll': poll, 'questions': questions}
    )


def poll_statistics(request):
    poll_results = PollResult.objects.filter(user=request.user)
    return render(
        request,
        'polls/poll_statistics.html',
        {'poll_results': poll_results}
    )
