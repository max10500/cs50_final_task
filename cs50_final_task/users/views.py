from django.contrib.auth import (
    login as auth_login,
    logout as auth_logout,
    authenticate,
)
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

from polls.models import PollResult


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            auth_login(request, user)
            return redirect('poll-list')
    else:
        form = UserCreationForm()

    return render(request, 'registration/signup.html', {'form': form})
