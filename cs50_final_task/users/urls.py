from django.urls import path, include

from . import views


urlpatterns = [
    path('accounts/', include([
        path('', include('django.contrib.auth.urls')),
        path('signup/', views.signup, name='signup'),
    ])),
]
